tests = test/ test/routes test/metadata_extractors
test:
	@NODE_ENV=test ./node_modules/.bin/mocha $(tests) --noinject

test-cov: 
	@NODE_ENV=test ./node_modules/.bin/mocha $(tests) --reporter html-cov --coverage > coverage.html

clean:
	rm -rf norchindex* store store-test
	mongo ebooker-test --eval "db.dropDatabase()"
	mongo ebooker --eval "db.dropDatabase()"

.PHONY: clean test
