var utils = require('../utils');

var extractMetadata = function(pdf, callback) {
  var name = pdf.file.name;

  // maybe we have something
  pdf.book.title = pdf.book.title || utils.trim(name.replace(/\.pdf/, '')
                                              .replace(/(\+|_)+/g, ' '));
  pdf.book.author =  pdf.book.author || [];
  
  return callback(null, pdf);
}


exports.extractMetadata = extractMetadata;