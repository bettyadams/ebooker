var pdfParser = require('pdfinfo');

var extractMetadata = function(pdf, callback) {
  var path = pdf.file.path
    , pdfInfo = pdfParser(path);

  pdfInfo.info(function(err, result) {
    if (err) {
      return callback(err, null);
    } else {
      pdf.book.title = result.title ? result.title : pdf.book.title;
      pdf.book.author = result.author ? [ result.author ] : pdf.book.author;
      return callback(null, pdf);
    }
  });
}

exports.extractMetadata = extractMetadata;