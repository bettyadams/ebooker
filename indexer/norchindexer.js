var indexer = require('search-index');

var index = function(object, identifier, options, callback) {

  options.filters = options.filters || [];

  indexer.index(JSON.stringify(object), identifier, options.filters, function(msg){
    return callback(null, msg);
  });
}

var remove = function(id, callback) {
  return indexer.deleteDoc(id, function(msg){
    return callback(null, msg);
  });
}

var search = function(query, callback) {
  return indexer.search(query, function(results) {
    return callback(
      results.hits.map(function(result) {
        return result.document.id;
      })
    );
  }); 
}

exports.index = index;
exports.remove = remove;
exports.search = search;