// Force test environment
process.env.NODE_ENV = 'test';

// Run $ expresso
var request = require('supertest')
  , chai = require('chai')
  , config = require('../../config')
  , rimraf = require('rimraf')
  , async = require('async')
  , mongoose = require('mongoose')
  , request = require('supertest')
  , app = require('../../app')
  , path = require('path')
  , fs = require('fs')
  , testConfig = require('../testConfig')
  , assert = chai.assert;

chai.Assertion.includeStack = true; 

describe('POST /documents.json', function() {

  this.timeout(20000);

  beforeEach(function(done){
    async.parallel(
      [
        function(callback) {
          fs.readdir(config.storage, function(er, files) {
            async.map(files, async.apply(rimraf), callback);
          })
        },
        function(callback) {
          mongoose.connection.db.dropDatabase(callback);        
        }
      ],
      done);
  });
  it ('upload should succeed', function(done) {
    request(app.server)
      .post('/upload')
      .attach('fileToUpload', path.join(testConfig.fixtures, 'hasdata.pdf'))
      .end(function(err, res){
        if (err) return done(err);
        request(app.server)
          .get('/books')
          .set('Content-Type', 'application/json')
          .end(function(err, res){
            if (err) return done(err);
            var uploadedBook = JSON.parse(res.text)
              , book = uploadedBook[0];
            console.dir(res.text);
            assert.strictEqual(uploadedBook.length, 1);
            assert.strictEqual(book.title, 'ANonExistentTitle');
            assert.sameMembers(book.author, ['ANonExistentAuthor']);

            done();
          });
      });
  });
});
