// Force test environment
process.env.NODE_ENV = 'test';

var chai = require('chai')
  , jsc = require('jscoverage')
  , amazon = jsc.require(module, '../amazon')
  , assert = chai.assert;

chai.Assertion.includeStack = true;

describe('Retrieving with title and author', function() {

  this.timeout(30000);

  it ('should return a populated object when a book is found', function(done){
    amazon.retrieve('Robert C. Martin', 'Clean Code: A Handbook of Agile Software Craftsmanship', function(err, result) {
      if (err) throw err;

      assert.isNotNull(result);
      assert.equal(result.title, "Clean Code: A Handbook of Agile Software Craftsmanship");
      assert.sameMembers(result.author, ["Robert C. Martin"]);
      assert.ok(result.cover);
      assert.ok(result.rating);
      assert.ok(result.description);

      done();
    })
  })

  it ('should return an empty object if no book is found', function(done){
    amazon.retrieve('Ymexer', 'Zubpalswq', function(err, result) {
      if (err) throw err;

      assert.isNull(result);
      
      done();
    })
  })

  it ('should return a book search with multiple authors correctly', function(done){

    var titleSearch = 'Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation'
      , titleSearchResult = 'Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation (Addison-Wesley Signature Series (Fowler))';

    amazon.retrieve('Jez Humble', titleSearch, function(err, result) {
      if (err) throw err;

      assert.isNotNull(result);
      assert.equal(result.title, titleSearchResult);
      assert.sameMembers(result.author, ['David Farley', 'Jez Humble']);
      assert.ok(result.cover);
      assert.ok(result.rating);
      assert.ok(result.description);

      amazon.retrieve('David Farley', titleSearch, function(err, result) {
        if (err) throw err;

        assert.isNotNull(result);
        assert.equal(result.title, titleSearchResult);
        assert.sameMembers(result.author, ['David Farley', 'Jez Humble']);
        assert.ok(result.cover);
        assert.ok(result.rating);
        assert.ok(result.description);
        
        done();
      })
    })
  })

  it ('should return a book search with a single author correctly', function(done){
    amazon.retrieve('', 'Pragmatic Project Automation', function(err, result) {
      if (err) throw err;

      assert.isNotNull(result);
      assert.equal(result.title, "Pragmatic Project Automation: How to Build, Deploy, and Monitor Java Apps");
      assert.sameMembers(result.author, ["Mike Clark"]);
      assert.ok(result.cover);
      assert.ok(result.rating);
      assert.ok(result.description);

      done();
    })
  })

});
