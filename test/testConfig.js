var path = require('path')
  , cwd = path.dirname(__filename);

exports.fixtures = path.join(cwd, 'fixtures');