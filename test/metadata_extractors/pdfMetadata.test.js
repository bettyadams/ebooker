// Force test environment
process.env.NODE_ENV = 'test';

var chai = require('chai')
  , jsc = require('jscoverage')
  , path = require('path')
  , assert = chai.assert
  , testConfig = require('../testConfig')
  , pdfparser = jsc.require(module, '../../metadata_extractors/pdfMetadata');

chai.Assertion.includeStack = true; 

describe('Parsing PDF info', function() {

  this.timeout(10000);

  var pdfFixture = function(pdf) {
    return {
      'file' : {
        'path': path.join(testConfig.fixtures, pdf)
      },
      'book' : {
        'title': '',
        'author': []
      }
    }
  }

  it ('should return an object with title and author even if no title or author is specified', function(done){
    pdfparser.extractMetadata(pdfFixture('nodata.pdf'), function(err, result) {
      if (err) throw err;
      assert.isNotNull(result.book);
      assert.strictEqual(result.book.title, '');
      assert.sameMembers(result.book.author, []);

      done();
    })
  })

  it ('should return an object with title and author if a title or author is specified', function(done){
    pdfparser.extractMetadata(pdfFixture('hasdata.pdf'), function(err, result) {
      if (err) throw err;

      assert.isNotNull(result.book);
      assert.strictEqual(result.book.title, 'ANonExistentTitle');
      assert.sameMembers(result.book.author, [ 'ANonExistentAuthor' ]);

      done();
    })
  })
});
