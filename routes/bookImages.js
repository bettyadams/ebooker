var Book = require('../models/book')
  , config = require('../config')
  , utils = require('../utils')
  , fs = require ('fs')
  , http = require('http')
  , os = require('os')

 if (/^Win.*/i.exec(os.type())) {
  var jpgExtracter = require('../convert')
    , extractOptions = ['-quality', '85', '-density', '400', '-resize', '300x400' ];
} else {
  var jpgExtracter = require('../pdftocairo')
    , extractOptions = ['-singlefile', '-jpeg', '-scale-to-x', '300', '-scale-to-y', '400'];

}

var defaultImage =function(response) {
  response.sendfile(path.join(config.storage, 'defaultImage.jpg'));
}

var show = function(req, response) {
  var id = req.params.id;
  if (id) {
    var path = utils.getFilePathFromId(id, 'jpg');
    fs.exists(path, function(exists) {
      if (exists) {
        return response.sendfile(path);
      } else {
        Book.findById(id, 'cover', { lean: true }, function(err, result) {
          if (err || !result) {
            return response.render('error', { 'error' : 'Unable to retrieve image for book ' + id + ' - ' + err });
          } else if (!result.cover) {
            var pdfPath = utils.getFilePathFromId(id)
              , converter = jpgExtracter(pdfPath);
		    
            converter.add_options(extractOptions);
			
            converter.convert(function(err, result) {
              if (err) {
                return response.render('error', { 'error' : 'Unable to retrieve image for book ' + id + ' - ' + err });
              } else {
                return response.sendfile(path);
              }
            });
          } else {
            http.get(result.cover, function(res) {
              var pathStream = fs.createWriteStream(path)
                  .on("finish", function(ex) {
                    return response.sendfile(path);
                  })
                  .on("error", function(ex) {
                    fs.unlink(path);
                    return response.render('error', { 'error' : 'Unable to retrieve image for book ' + id + ' - ' + ex });
                  });
              res.pipe(pathStream);
            });

          }
        }); 

      }
    });
  } else {
    return defaultImage(response);
  }
};


exports.show = show;
