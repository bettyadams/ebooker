var config = require('../config')
  , utils = require ('../utils')
  , Book = require('../models/book');

var allowedTypes = [ 'pdf', 'txt' ]
  , defaultType = 'pdf';

var getFilename = function(book, type) {
  var fileName = (book.author) ? 
    (book.title + ' - ' + book.author) : book.title;
  return fileName + '.' + type;
}

var show = function(req, res) {
  var id = req.params.id;
  var type = req.params.type;

  if (!type) {
    type = defaultType;
  }

  if (allowedTypes.indexOf(type) == -1) {
    return res.render('error', { 'error' : type + ' is not allowed, must be in ' + allowedTypes})
  }

  if (id) {
    Book.findById(id, 'title author', function(err, book) {
      if (err || book == null) { 
        return res.render('error', { 'error': 'Unable to retrieve file ' + err }); 
      } else {
        return res.download(utils.getFilePathFromId(id, type), getFilename(book, type));
      }
    })
  } else {
    return res.render('error', { 'error': 'No id specified' });
  }
}

exports.show = show;