var Book = require('../models/book')
  , amazon = require('../amazon')
  , utils = require('../utils')
  , fs = require('fs')
  , indexer = require('../indexer/norchindexer');

var getQuery = function(req) {
  //default values
  var offsetDefault = 0;
  var pageSizeDefault = 999999999;

  var q = {};
  q['query'] = "*";
  if (req.query['q']) {
    q['query'] = req.query['q'].toLowerCase().split(' ');
  }
  if (req.query['searchFields']) {
    q['searchFields'] = req.query.searchFields;
  }
  if (req.query['offset']) {
    q['offset'] = req.query['offset'];
  } else {
    q['offset'] = offsetDefault;
  }
  if (req.query['pageSize']) {
    q['pageSize'] = req.query['pageSize'];
  } else {
    q['pageSize'] = pageSizeDefault;
  }
  if (req.query['facets']) {
    q['facets'] = req.query['facets'].toLowerCase().split(',');
  }
  if (req.query['weight']) {
    q['weight'] = req.query.weight;
  }
  //&filter[topics][]=cocoa&filter[places][]=usa
  if (req.query['filter']) {
    q['filter'] = req.query.filter;
  }
  console.log(q);
  return q;
}

var either = function(request, response, error, success) {
  return function(err, result) {
    if (err) {
      return error(request, response, err)
    } else {
      return success(request, response, result)
    }
  }
}

var defaultError = function(errorMsg) {
  return function(request, response, error) {
    return response.render('error', { 'error' : errorMsg + ' - ' + error });    
  };
};

var defaultSuccess = function(request, response, result) {
  if (result == null) {
    return response.send(404);
  } else {
    return response.send(result);  
  }
}

var index = function(req, res) {
  var queryParam = req.query.q;
  if (queryParam && utils.trim(queryParam).length > 2) {
    var q = getQuery(req);
    return indexer.search(q, function(matchingIds) {
      return Book.find({ '_id' : { '$in' : matchingIds }}, 'title author rating cover _id description', { lean: true }, 
        either(req, res, defaultError('Unable to retrieve books for listing'), defaultSuccess)
      );
    });
  } else {
    return Book.find({}, 'title author rating cover _id description', { lean: true }, 
      either(req, res, defaultError('Unable to retrieve books for listing'), defaultSuccess)
    );
  }

}

var show = function(req, res) {
  var id = req.params.id;
  if (id) {
    return Book.findById(id, 'title author rating cover _id description', { lean: true },
      either(req, res, defaultError('Unable to retrieve book for listing'), defaultSuccess)
    ); 
  } else {
    return defaultError('No id was specified')(request, response, '');
  }
}

var update = function(req, res) {
  var id = req.params.id;
  if (id) {
    Book.findById(id, 'title author rating cover _id description', { lean: true }, 
      either(req, res, defaultError('Unable to retrieve book for updating'), function(request, response, result) {
        var newObj = request.body;

        for (var key in result) {
          if (key != '_id' && newObj.hasOwnProperty(key)) {
            result[key] = newObj[key];
          }
        }
        return Book.save(result, either(request, response, defaultError('Unable to update book'), function(request, response, savedResult) {
          var filename = utils.getFilePathFromId(id, 'txt');
          fs.readFile(filename, 'utf8', function(err, data) {
            if (err) {
              console.error('Warning, could not read file for reindexing. Search indexes may be out of date');
            } else {
              Book.updateIndex(savedResult, data);
            } 
          });              
          return defaultSuccess(request, response, savedResult);
        }));
      })
    ); 
  } else {
    return defaultError('No id was specified')(request, response, '');
  }
}

var refresh = function(req, res) {
  var id = req.params.id;
  if (id) {
    Book.findById(id, 'title author rating cover description', { lean: true },
      either(req, res, defaultError('Unable to find book to refresh'), function(request, response, book) {
        amazon.retrieve(book.author, book.title, function(err, result) {
          if (!err && result !== null) {
            book.title = result.title;
            book.author = result.author;
            book.rating = result.rating;
            book.cover = result.cover;
            book.description = result.description;
            return Book.save(book, either(request, response, defaultError('Could not refresh book'), function(request, response, savedResult) {
              var filename = utils.getFilePathFromId(id, 'txt');
              fs.readFile(filename, 'utf8', function(err, data) {
                if (err) {
                  console.error('Warning, could not read file for reindexing. Search indexes may be out of date');
                } else {
                  Book.updateIndex(savedResult, data);
                } 
              });              
              Book.clearCover(id, savedResult, either(request, response, defaultError('Could not remove cover'), defaultSuccess));
            }));
          } else {
            return defaultSuccess(request, response, book);  
          }
        });
      })
    );
  } else {
    return defaultError('No id was specified')(request, response, '');
  }
}

var remove = function(req, res) {
  var id = req.params.id;
  if (id) {
    Book.findById(id, '_id', 
      either(req, res, defaultError('Unable to find book to remove'), function(request, response, result) {
        return Book.remove(result, either(request, response, defaultError('Could not remove book'), function(request, response, savedResult) {
          Book.removeStore(id, savedResult, either(request, response, defaultError('Could not remove cover'), defaultSuccess));
        }));
      })
    );
  } else {
    return defaultError('No id was specified')(request, response, '');
  }
}

exports.index = index;
exports.show = show;
exports.update = update;
exports.remove = remove;
exports.refresh = refresh;
