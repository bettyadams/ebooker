var spinner = new Spinner();
var url = '/upload';

angular.module('SpinnerService', [])
    .config(function ($httpProvider) {
        $httpProvider.responseInterceptors.push('spinnerInterceptor');
        var spinnerFunction = function (data, headersGetter) {
            var target = document.getElementById('bookView');
            spinner.spin(target);
            return data;
        };
        $httpProvider.defaults.transformRequest.push(spinnerFunction);
    })
    // register the interceptor as a service, intercepts ALL angular ajax http calls
    .factory('spinnerInterceptor', function ($q, $window) {
        return function (promise) {
            return promise.then(function (response) {
                spinner.stop();
                return response;

            }, function (response) {
                spinner.stop();
                return $q.reject(response);
            });
        };
    })

angular.module('StorageService', [])    
    .factory('storage', function () {
      var store = {};
      var storageFacility = {
        get: function(key) {
          return store[key];
        },
        set: function(key, val) {
          store[key] = val;
        }
      };
      return storageFacility;
    });


angular.module('ebooker', ['SpinnerService', 'blueimp.fileupload', 'StorageService'])
  .config(['$routeProvider', 'fileUploadProvider', function($routeProvider, fileUploadProvider) {
    $routeProvider.
      when('/books', { templateUrl: 'partials/book-list.html', controller: 'BookListCtrl', reloadOnSearch: false }).
      when('/books-small', { templateUrl: 'partials/book-list-small.html', controller: 'BookListCtrl', reloadOnSearch: false}).
      otherwise({redirectTo: '/books'});

    angular.extend(fileUploadProvider.defaults, {
      acceptFileTypes: /(\.|\/)(pdf)$/i
    });

  }])
  .controller('EBookerFileUploadController', [
    '$scope', 'fileUpload',
    function ($scope, fileUpload) {
      $scope.options = {
        url: url
      };
      $scope.clear = function() {
        $scope.queue.length = 0;

      };
    }
  ])
  .controller('BookListCtrl', ['$scope', '$http', '$location', '$timeout', '$route', '$rootScope', 'storage',
    function($scope, $http, $location, $timeout, $route, $rootScope, storage) {
     
    var onMobile = function() {
      return !matchMedia('only screen and (min-width: 994px)').matches;
    }

    var onListView = function() {
      return ($location.path() === '/books-small' || storage.get('listType') === 'list');
    }

    var numCols = (onMobile() || onListView()) ? 1 : 6
      , titleMaxLength = 35;
    
    $scope.query = '';
    $scope.selectedId = $location.search()['id'];
    $scope.selectedRow = -1;


    $scope.getSearchType = function() {
      return storage.get('searchType');
    }

    $scope.setSearchType = function(searchType) {      
      storage.set('searchType', searchType);
      $scope.refreshList($scope.searchTerms, function(data)  {
        populateScopeWithBooks(data);
      });
    }

    $scope.refreshList = function(terms, success){
      var searchOptions = {
          'q' : terms,
          'weight': {
            'title': 10,
            'author': 9,
            'body': 5
          }

        };
      switch($scope.getSearchType()) {
        case 'Text':
          searchOptions['searchFields[]'] = ['body'];
          break;
        case 'Title':
          searchOptions['searchFields[]'] = ['title'];
          break;
        case 'Author':
          searchOptions['searchFields[]'] = ['author'];
          break;
        default:
          break;
      }

      $http.get('/books', { 'params': searchOptions }).success(success)
    };

    if (!$scope.getSearchType()) {
      $scope.setSearchType('Everything');
    }


    var populateScopeWithBooks = function(rawData) {
      var numRows = Math.ceil(rawData.length / numCols)
        , rows = [];

      for (var i = 0; i < numRows; ++i) {
        rows.push(rawData.splice(0, numCols));
      }
      $scope.books = rows;    
    }

    var tempSearchText = '',
      searchTimeout;

    $scope.$watch('searchTerms', function (val) {
        if (searchTimeout) {
          $timeout.cancel(searchTimeout);
        }

        tempSearchText = val;
        searchTimeout = $timeout(function() {
          $scope.refreshList(tempSearchText, function(data)  {
            populateScopeWithBooks(data);
          });
        }, 200);
    })

    $rootScope.$on('$locationChangeStart', function(event, nextLocation, currentLocation) {
      var hashStart = nextLocation.indexOf("#");
      var hash = (hashStart != -1) ? nextLocation.substring(hashStart + 1) : "";
      if (hash === '/books' && storage.get('listType') === 'list') {
        $location.path('/books-small');
      }
    });

    $scope.refreshList('', function(data) {
      populateScopeWithBooks(data);
      if ($scope.selectedId) {
        var row = getRowForBook($scope.selectedId);
        $scope.showBook($scope.selectedId, row);
      }

    });

    $scope.books = [];

    $scope.$on('fileuploadstop', function(e, args) {
      $route.reload();
    });

    var getRowForBook = function(id) {
      for (var i = 0; i < $scope.books.length; ++i) {
        for (var j = 0; j < $scope.books[i].length; ++j) {
          if (id == $scope.books[i][j]._id.toString()) {
            return i;
          }
        }
      }
      return -1;
    }

    $scope.$location = $location;

    $scope.prettyPrintAuthor = function(authors) {
      if (authors.length < 2) {
        return authors[0];
      } else {
        var prevAuthors = authors.slice(0, -1)
          , lastAuthor = authors.slice(-1);

        return prevAuthors.join(', ') + ' and ' + lastAuthor;
      }
    }   

    $scope.truncate = function(title, maxLength) {
      var clippedTitle = title.slice(0, maxLength);   
      if (title.length > maxLength) {
        clippedTitle += '...'
      }
      return clippedTitle;
    }   

    $scope.showList = function() {
      storage.set('listType', 'list');
      $location.path('/books-small');
    }

    $scope.showThumbs = function() {
      storage.set('listType', 'thumbs');
      $location.path('/books');
    }

    $scope.showBook = function(id, row) {
      $scope.selectedId = id;
      $scope.selectedRow = row;
      $timeout(function() {   
        $scope.$broadcast('bookSelected', id, row);
      });
    }

    $scope.clearSelection = function() {
      if ($scope.selectedId) {
        $location.search('id', null);
        $scope.selectedId = null;
        $scope.selectedRow = -1;
      }
    }

    $scope.getBook = function(id) {
      for (var i = 0; i < $scope.books.length; ++i) {
        for (var j = 0; j < $scope.books[i].length; ++j) {
          if (id == $scope.books[i][j]._id.toString()) {
            return $scope.books[i][j];
          }
        }
      }
    }

    $scope.setBook = function(id, val) {
      for (var i = 0; i < $scope.books.length; ++i) {
        for (var j = 0; j < $scope.books[i].length; ++j) {
          if (id == $scope.books[i][j]._id.toString()) {
            $scope.books[i][j] = val;
          }
        }
      }
    }

    $scope.getSelectedBook = function() {
      return getSelectedBook($scope.selectedId);
    }

  }])
  .controller('BookDetailCtrl', ['$scope', '$http', '$location', '$routeParams', '$timeout', '$route',
      function($scope, $http, $location, $routeParams, $timeout, $route) {
     
    var bookId = $location.search()['id'] || $routeParams.id;

    // from http://stackoverflow.com/a/7557433
    var isElementInViewport = function(el) {
      var rect = el.getBoundingClientRect();

      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document. documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document. documentElement.clientWidth) /*or $(window).width() */
      );
    }

    $scope.prettyPrintAuthor = function(authors) {
      if (!authors) {
        return '';
      } else if (authors.length < 2) {
        return authors[0];
      } else {
        var prevAuthors = authors.slice(0, -1)
          , lastAuthor = authors.slice(-1);

        return prevAuthors.join(', ') + ' and ' + lastAuthor;
      }
    }   

    $scope.save = function(editForm) {
      if (editForm.$valid) {
        $http.put('/books/' + bookId, $scope.book).success(function(data) {          
          $scope.setBook(data._id, data);
          if (!(data.author && data.author.length > 0 && data.description)) {
            $scope.refresh();
          }
          if (data._id == $scope.selectedId) {
            $scope.book = data;
            $scope.editMode = false;  
          }
        });
      }
    }
     
    $scope.$on('bookSelected', function(e, id, row) {
      bookId = id;
      refreshBookData();
      $scope.editMode = false;
      var d = document.getElementById('book-row-' + row);
      if (!isElementInViewport(d)) {
        $("html, body").animate({ scrollTop: jQuery(d).position().top });
        $location.search('id', id);
      }

    });

    var refreshBookData = function() {
      $http.get('/books/' + bookId).success(function(data) {
        $scope.setBook(data._id, data);
        if (data._id == $scope.selectedId) {
          $scope.book = data;
          $scope.bookPristine = angular.copy(data);
        }
      }).error(function(data) {
        $location.path('/books');
      });
    }

    $scope.edit = function() {
      $scope.editMode = true;
    }

    $scope.refresh = function() {
      $http.put('/books/' + bookId + '/refresh').success(function(data) {        
        $scope.setBook(data._id, data);
        if (data._id == $scope.selectedId) {
          $scope.book = data;
          $scope.bookPristine = angular.copy(data);
          $scope.editMode = false;
        }
      });

    }

    $scope.cancel = function() {
      $scope.book = angular.copy($scope.bookPristine);
      $scope.editMode = false;    
    }

    $scope.remove = function() {
      $http.delete('/books/' + bookId).success(function(data) {
        $location.search('id', null);
        $route.reload();
      });
    }

    if (bookId) {
      refreshBookData(bookId);
    }

  }]);