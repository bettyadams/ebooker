var path = require('path');
var mainCwd = path.dirname(__filename)

if (process.env.NODE_ENV !== 'test') {
  var storage = path.join(mainCwd, 'store')
    , dbUrl = 'mongodb://localhost/ebooker';
} else {
  var storage = path.join(mainCwd, 'store-test')
    , dbUrl = 'mongodb://localhost/ebooker-test';
}

exports.storage = storage;
exports.dbUrl = dbUrl;

