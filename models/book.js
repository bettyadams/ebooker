var mongoose = require('mongoose')
  , fs = require('fs')
  , utils = require('../utils')
  , path = require('path')
  , rimraf = require('rimraf')
  , indexer = require('../indexer/norchindexer');

var db = mongoose.connection;


var bookSchema = mongoose.Schema({
  title:  { type: String, index: true },
  author: [{ type: String, index: true }],
  rating: String,
  cover: String,
  filename: String,
  description: String,
  md5: { type: String, index: true }
});

var BookModel = mongoose.model('Book', bookSchema);

function Book(title, author, cover, rating, filename, description, md5, id) {
  this.title = title;
  this.author = author;
  this.cover = cover;
  this.rating = rating;
  this.filename = filename;
  this.description = description;
  this.md5 = md5;
  if (id) {
    this._id = id;
  }
}

var convertBookModelToBook = function(result) {
  return new Book(result.title, result.author, result.cover, result.rating, result.filename, result.description, result.md5, result._id);  
}

var convertCallback = function(callback) {
  return function(err, result) {
    if (err || !result) { 
      return callback(err, null); 
    }

    if (Array.isArray(result)) {
      return callback(null, result.map(convertBookModelToBook));
    }

    return callback(null, convertBookModelToBook(result));
  }

}

var findById = function(id, fields, options, callback) {
  BookModel.findById(id, fields, options, convertCallback(callback));
}

var findOne = function(criteria, fields, options, callback) {
  BookModel.findOne(criteria, fields, options, convertCallback(callback));
}

var find = function(criteria, fields, options, callback) {
  BookModel.find(criteria, fields, options, convertCallback(callback));
}

var save = function(book, callback) {
  if (book._id) {
    BookModel.findById(book._id, function (err, res) {
      BookModel.schema.eachPath(function(key, type) {
        if (key.starts != '_id' && (typeof book[key] != 'undefined')) {
          res[key] = book[key];
        }
      });
      res.save(convertCallback(callback));
    });    
  } else {
    var bookToSave = new BookModel(book);
    bookToSave.save(convertCallback(callback));
  }
}

var removeIndex = function(id, callback) {
  indexer.remove(id, callback);
}

var remove = function(book, callback) {
  BookModel.findByIdAndRemove(book._id, convertCallback(function(error, result) {
      if (!error) {
        removeIndex(book._id, function(err, res) {
          return callback(error, result);
        })      
      } else {
        return callback(error, result)
      }
    })
  );
}

var create = function() {
  return new Book('', [], '', '', '', '', '', '');
}

var clearCover = function(id, result, callback) {
  var cover = utils.getFilePathFromId(id, 'jpg');
  fs.unlink(cover, function(err) {
    return callback(err, result);
  });
}

var removeStore = function(id, result, callback) {
  var cover = utils.getFilePathFromId(id, 'jpg');
  rimraf(path.dirname(utils.getFilePathFromId(id, 'jpg')), function(err) {
    return callback(err, result);
  });
}

var createForIndexing = function(book, text) {

  var idString = book._id.toString()
    , indexRecord = {};
  
  indexRecord[book._id.toString()] = {
    "title": book.title,
    "author": book.author.join(' '),
    "rating": book.rating,
    "body": text
  };
  if (book.description) { 
    indexRecord[idString]['description'] = book.description; 
  };

  return indexRecord;
}

var updateIndex = function(book, text) {
  var doc = createForIndexing(book, text);
  indexer.index(doc, book._id, { }, function(err, result){
    console.log('Indexing update finished for ' + book._id + ' - ' + result);
  });
}

exports.findById = findById;
exports.findOne = findOne;
exports.find = find;
exports.save = save;
exports.create = create;
exports.clearCover = clearCover;
exports.remove = remove;
exports.removeStore = removeStore;
exports.createForIndexing = createForIndexing;
exports.removeIndex = removeIndex;
exports.updateIndex = updateIndex;