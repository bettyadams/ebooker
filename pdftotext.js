var pdftotext = require('pdftotextjs')
  , async = require('async');

var convert = function(pdfFile, callback) {  
  var pdf = pdftotext(pdfFile); 
  pdf.add_options(['-nopgbrk']);
  pdf.getText(function(err, text, params){
    if (err) {
      callback(err, null);
    } else {
      callback(null, text);
    }
  });
}


exports.convert = convert;