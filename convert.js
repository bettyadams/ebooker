var shell = require('shelljs');

// we can only do like this because of a bug in shelljs
// global silent
shell.config.silent = true;

function convert(filename, options) {
  this.options = options || {};
  //quote filename
  this.options.additional = ['"' + filename + '.pdf[0]"'];
  this.filename = filename;
  
  convert.prototype.add_options = function(optionArray) {
    if (typeof optionArray.length !== undefined) {
        var self = this;
        optionArray.forEach(function(el) {
          if (el.indexOf(' ') > 0) {
            var values = el.split(' ');
            self.options.additional.push(values[0], values[1]);
          } else {
            self.options.additional.push(el);
          }
        });
    }
    return this;
  };

  convert.prototype.convert = function(cb) {
    var self = this;
	self.add_options(['"' + self.filename + '.jpg"']);
	console.error(self.options.additional.join(' '));
    var child = shell.exec('convert ' + self.options.additional.join(' '), function(code, data) {
      if (code === 0) {
        if (cb && typeof cb === "function") {
          cb(null, data, self.options.additional);
        }
      }
      else {
        var err;
        if (!shell.which('convert')) {
          err = new Error('convert (poppler-utils) is missing. Hint: sudo apt-get install imagemagick / brew install imagemagick / port install imagemagick');
        }
        else {
		  console.error(self.options.additional.join(' ') + data);
          err = new Error(data);
        }
        if (cb && typeof cb === "function") {
          cb(err, data, self.options.addtional);
        }
      }
    });
  }

  convert.prototype.error = function(callback) {
    this.options.error = callback;
    return this;
  };

  convert.prototype.success = function(callback) {
    this.options.success = callback;
    return this;
  };
}

// module exports
exports = module.exports = function(filename, args) {
  return new convert(filename, args);
};
