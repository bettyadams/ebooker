var cheerio = require('cheerio')
  , http = require('http')
  , url = require('url')
  , queryString = require('querystring')
  , utils = require('./utils');


var ratingRegex = /([0-9]+(\.[0-9])*) out of ([0-9]) stars/
  , lookInsideRegex = /(_PIsitb.*|_PJlook-inside.*)\.jpg$/;

var convertRatingFromStarsToPercent = function(ratingText) {
  var ratingMatch = ratingRegex.exec(ratingText);
  if (ratingMatch) {
    return (parseFloat(ratingMatch[1]) / parseFloat(ratingMatch[3])).toFixed(2); 
  } else {
    return 'Unknown';
  }
}

var removeLookInsideArrowFromCover = function(cover) {
  return cover ? cover.replace(lookInsideRegex, '.jpg') : cover;
}

var parseBook = function(title, bookUrl, callback) {

  var options = url.parse(bookUrl);
  options['headers'] = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36'
  };
  var req = http.get(options, function(res) {
    
    var body = '';

    res.on('data', function(chunk){      
      body += chunk;
    }).on('end', function(){      
      var authors = []
        , rawRating = ''
        , rawCover = ''
        , description = ''
        , parsedBook = null;

      var $ = cheerio.load(body);
      var $bookData = $('#handleBuy');
      if ($('.a-container').length == 0) {
        if ($bookData.length == 0) {
          $bookData = $('body');
        }
        rawRating = $bookData.find('.crAvgStars .swSprite').first().text();
        rawCover = $('#main-image').first().attr('src');
        authors = $bookData.find('.parseasinTitle + span > a')
                   .map(function(idx, elt) { 
                      return utils.trim($(elt).text()); 
                    })
                  .concat($bookData.find('.contributorNameTrigger')
                    .map(function(idx, elt) {
                      return utils.trim($(elt).text()); 
                    })
                  );
        description = utils.trim($('#postBodyPS').first().text());
        console.log(authors + " - " + $bookData.find('.parseasinTitle + span > a'));

      } else {
        var $bookContainer = $('.a-container').first();
        rawRating = $bookContainer.find('#averageCustomerReviews reviewCountTextLinkedHistogram').first().attr('title');
        rawCover = $('#imgBlkFront').first().attr('src');
        authors = $bookContainer.find('.author')
                    .map(function(idx, elt) {
                      var $elt = $(elt)
                        , $authorElt = $elt.find('a[data-asin]');

                      var authorName = ($authorElt.length > 0) ? 
                        $authorElt.text() : $elt.find('.a-link-normal').text();

                      return utils.trim(authorName);
                    });
        description = utils.trim($('#postBodyPS').first().text());
      }
      var rating = convertRatingFromStarsToPercent(rawRating)
        , cover = removeLookInsideArrowFromCover(rawCover);

      if (!description) {
        var descriptionHtml = $('#bookDescription_feature_div noscript').text();
        if (descriptionHtml) {
          description = utils.trim(cheerio.load(descriptionHtml).root().text());
        }
      }

      parsedBook = {
        'title': title,
        'author': authors,
        'rating': rating,
        'cover': cover,
        'description': description
      };        
      return callback(null, parsedBook);

    })
  }).on('error', function(e) {
    console.error(e);
    callback(e, null);
  });

}

var doSearch = function(author, title, callback) {
  var queryParams = {
    'sort': 'relevanceexprank',
    'search-alias': 'stripbooks',
    'unfiltered': '1',
    'field-title': title,    
  };

  if (author) {
    queryParams['field-author'] = author;
  }

  var options = {
    'host': 'www.amazon.com',
    'port': 80,
    'path': '/s/?' + queryString.stringify(queryParams),
    'headers': {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36'
    }
  }

  var req = http.get(options, function(res) {
    
    var body = '';

    res.on('data', function(chunk){      
      body += chunk;
    }).on('end', function(){      
      var $ = cheerio.load(body)
        , $results = $('#atfResults .result')

      if ($results.length !== 0) {
        var $result = $results.first().find('.data a.title')
          , title = $result.find('> span').attr('title') || $result.text()
          , resultUrl = $result.attr('href');

        return callback(null, utils.trim(title), resultUrl);
      } else {
        return callback(null, null, null);
      }
    })
  }).on('error', function(e) {
    callback(e, null, null);
  });
  console.log(req.path);

}

var retrieve = function(author, title, callback) {
  doSearch(author, title, function(err, title, url) {

    if (err || !title) {
      return callback(null, null);      
    }
    return parseBook(title, url, callback);
  });

}

exports.retrieve = retrieve;
