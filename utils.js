var fs = require('fs')
  , config = require('./config')
  , path = require('path')
  , crypto = require('crypto');

var trim = function(string) { 
  return string.replace(/^\s*|\s*$/g, '') 
}

var isError = function(obj) {
  return (obj instanceof Error);
}

var identityCallback = function(result) {
  return function(callback) {
    return callback(null, result);
  };
}

var createIfNotExists = function(path, callback) {
  fs.exists(path, function(exists) {
    if (!exists) {
      fs.mkdir(path, 0755, callback);
    } else {
      callback(null);
    }
  });
}

var getFilePathFromId = function(idString, type) {
  var suffix = type ? '.' + type : '';  
  return path.join(config.storage, idString, idString + suffix);
}

var generateHash = function(filePath, type, callback) {
  var hash = crypto.createHash(type);

  var s = fs.ReadStream(filePath);
  s.on('data', function(d) {
    hash.update(d);
  });
  s.on('end', function() {    
    callback(null, hash.digest('hex'));
  });
}


// When using async, we often want map/parallel to continue
// even when a single work unit fails or throws an error. 
// This callback function will convert an err to a result,
// wrapping it in an Error if not already done so.
var convertErrToResultErrorHandler = function(callback) {
  return function(err, result) {
    if (err) {
      console.error(err);
      if (!isError(err)) {          
        return callback(null, new Error(err.toString()));
      } else {
        return callback(null, err);
      }
    } else {
      return callback(null, result);
    }
  }
}

var logErrorResultHandler = function(callback) {
  return function(err, result) {
    if (err) {
      console.log(err);
    } else {
      callback(null, result);
    }
  }
}

exports.trim = trim;
exports.isError = isError;
exports.identityCallback = identityCallback;
exports.createIfNotExists = createIfNotExists;
exports.getFilePathFromId = getFilePathFromId;
exports.generateHash = generateHash;
exports.convertErrToResultErrorHandler = convertErrToResultErrorHandler;